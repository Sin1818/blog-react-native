import AsyncStorage from "@react-native-async-storage/async-storage";
import Icon from "react-native-vector-icons/FontAwesome";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";

import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const Header = ({ username, token }) => {
  // Declaring state for hambeurger menu to be open or close
  const [isOpen, setIsOpen] = useState(false);

  // To navigate other pages
  const nav = useNavigation();

  // Navigat to all blogs page on the menu
  const handleBlogPage = () => {
    nav.navigate({
      name: "AllBlogs",
      params: { username: username, token: token },
      merge: false,
    });
  };

  // Navigat to Create page on the menu
  const handleCreateBlog = () => {
    nav.navigate({
      name: "CreateBlog",
      params: { username: username, token: token },
      merge: false,
    });
  };

  // Navigat to users blog page on the menu
  const handleUserBlog = () => {
    nav.navigate({
      name: "Blog",
      params: { username: username, token: token },
      merge: false,
    });
  };

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  // Logout function
  const handleLogout = async () => {
    try {
      // Clear user session or token from AsyncStorage
      await AsyncStorage.removeItem("token"); // Replace 'token' with your actual token key

      // Navigate to the login or authentication screen
      nav.navigate("Home"); // Replace 'Login' with your desired screen name
    } catch (error) {
      console.log("Error while logging out:", error);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={toggleMenu} style={styles.menuButton}>
        <Text style={styles.menuText}>{isOpen ? "X" : "☰"}</Text>
      </TouchableOpacity>
      {isOpen && (
        <View style={styles.menuContent}>
          <TouchableOpacity onPress={() => handleBlogPage()}>
            <View style={styles.textBorder}>
              <Icon name="file-text" size={20} color="#000000" />
              <Text style={styles.menuText}>All Blogs</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleUserBlog()}>
            <View style={styles.textBorder}>
              <Icon name="globe" size={20} color="#000000" />
              <Text style={styles.menuText}>My Page</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleCreateBlog()}>
            <View style={styles.textBorder}>
              <Icon name="book" size={20} color="#000000" />
              <Text style={styles.menuText}>Create Blogs</Text>
            </View>
          </TouchableOpacity>
        </View>
      )}
      <Text style={styles.userText}>
        <AntDesign name="user" size={17} color="black" />
        {"  "} {username} {"  "}{" "}
        <AntDesign
          name="logout"
          size={17}
          color="black"
          onPress={() => handleLogout()}
        />
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 16,
    backgroundColor: "#f9f9f9",
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    height: 100,
    borderBottomColor: "#000000",
    justifyContent: "space-between",
  },
  menuButton: {
    paddingTop: 30,
  },
  menuText: {
    fontSize: 17,
    fontWeight: "bold",
    color: "#333",
    marginLeft: 5,
  },
  menuContent: {
    position: "absolute",
    top: 60,
    left: 0,
    right: 0,
    padding: 10,
    width: "50%",
    marginTop: 40,
  },
  itemText: {
    fontSize: 60,
    fontWeight: "bold",
    color: "#333",
    borderBottomWidth: 5,
    borderBottomColor: "#ccc",
  },
  textBorder: {
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    paddingTop: 10,
    paddingBottom: 20,
    flexDirection: "row",
  },
  userText: {
    justifyContent: "center",
    fontSize: 17,
    fontWeight: "bold",
    marginTop: 20,
    textAlign: "right",
  },
});

export default Header;
