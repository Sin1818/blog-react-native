import AsyncStorage from "@react-native-async-storage/async-storage";
import Header from "../components/Header";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import React, { useEffect, useState } from "react";
import { SimpleLineIcons } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const BlogPage = ({ route }) => {
  const { username } = route.params; // Getting the username from the route to use on header

  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    fetchUserBlogs();
  });

  // Getting all user blog and token from backend
  const fetchUserBlogs = async () => {
    try {
      const token = await AsyncStorage.getItem("token");

      if (token) {
        const response = await fetch("http://192.168.10.123:5000/api/blogs", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        });
        const data = await response.json();

        const blogsWithDate = data.map((blog) => ({
          ...blog,
          createdAt: new Date(blog.createdAt).toLocaleString(),
        }));

        setBlogs(blogsWithDate);
      }
    } catch (error) {
      console.error(error);
    }
  };

  // Delete function
  const deleteBlog = async (id) => {
    try {
      const token = await AsyncStorage.getItem("token");

      if (token) {
        const response = await fetch(
          `http://192.168.10.123:5000/api/blogs/delete/${id}`,
          {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = await response.json();
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <Header username={username} />
      </View>
      <Text style={styles.pageTitle}>Blogs</Text>

      <ScrollView>
        {blogs.map((item) => (
          <View style={styles.blogPost}>
            <View key={item._id} style={styles.headStyle}>
              <Text style={styles.title}>{item.title}</Text>
              <Text style={styles.blogger}>
                Author:{"  "} {item.blogger} {" - "} {item.createdAt}
              </Text>

              <Text style={styles.content}>{item.content}</Text>

              <View style={styles.socialContainer}>
                <View style={styles.iconContainer}>
                  <SimpleLineIcons name="like" size={25} color="black" />
                  <FontAwesome5
                    name="comment"
                    size={25}
                    color="black"
                    style={styles.comment}
                  />
                  <MaterialCommunityIcons
                    name="delete-forever-outline"
                    size={30}
                    color="black"
                    style={styles.delete}
                    onPress={() => deleteBlog(item._id)}
                  />
                </View>
              </View>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "#ffffff",
  },
  pageTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    marginTop: 170,
  },
  blogList: {
    marginTop: 10,
  },
  blogPost: {
    marginBottom: 20,
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    borderRadius: 5,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 5,
  },
  blogger: {
    fontSize: 16,
    fontStyle: "italic",
    borderBottomWidth: 2,
    borderBottomColor: "#ccc",
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 10,
  },
  content: {
    fontSize: 16,
    borderBottomWidth: 2,
    borderBottomColor: "#ccc",
    paddingBottom: 20,
  },
  socialContainer: {
    paddingVertical: 8,
    paddingTop: 10,
  },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  comment: {
    marginLeft: 16,
  },
  delete: {
    marginLeft: 16,
    color: "red",
  },
});

export default BlogPage;
