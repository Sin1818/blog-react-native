import Header from "../components/Header";
import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { SimpleLineIcons } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";

const AllUsersBlogs = ({ route }) => {
  const { username } = route.params; // Getting the username from the route to use on header

  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    fetchBlogs();
  });

  const fetchBlogs = async () => {
    try {
      const response = await fetch("http://192.168.10.123:5000/api/blogs/all"); // calling backend
      const data = await response.json();

      // Converting create date from last to top on the list
      const blogsWithDate = data.map((blog) => ({
        ...blog,
        createdAt: new Date(blog.createdAt).toLocaleString(),
      }));

      setBlogs(blogsWithDate);
    } catch (error) {
      console.log("Error fetching blogs:", error);
    }
  };

  return (
    <View style={styles.container}>
      <View>
        <Header username={username} />
      </View>
      <Text style={styles.pageTitle}>All Blogs</Text>

      <ScrollView>
        {/* Useing map to read all data insreted from backend to frontend */}
        {blogs.map((item, index) => (
          <View style={styles.blogPost}>
            <View key={index} style={styles.headStyle}>
              <Text style={styles.title}>{item.title}</Text>
              <Text style={styles.blogger}>
                Author:{"  "} {item.blogger} {" - "} {item.createdAt}
              </Text>

              <Text style={styles.content}>{item.content}</Text>

              <View style={styles.socialContainer}>
                <View style={styles.iconContainer}>
                  <SimpleLineIcons name="like" size={25} color="black" />
                  <FontAwesome5
                    name="comment"
                    size={25}
                    color="black"
                    style={styles.comment}
                  />
                </View>
              </View>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "#ffffff",
  },
  pageTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    marginTop: 170,
  },
  blogList: {
    marginTop: 10,
  },
  blogPost: {
    marginBottom: 20,
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    borderRadius: 5,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
    marginBottom: 5,
  },
  blogger: {
    fontSize: 16,
    fontStyle: "italic",
    borderBottomWidth: 2,
    borderBottomColor: "#ccc",
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 10,
  },
  content: {
    fontSize: 16,
    borderBottomWidth: 2,
    borderBottomColor: "#ccc",
    paddingBottom: 20,
  },
  socialContainer: {
    paddingVertical: 8,
    paddingTop: 10,
  },
  iconContainer: {
    flexDirection: "row",
    // justifyContent: "space-between",
  },
  comment: {
    marginLeft: 160,
  },
  delete: {
    marginLeft: 16,
    color: "red",
  },
});

export default AllUsersBlogs;
