import Header from "../components/Header";
import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Text } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

const CreateBlogPage = ({ route }) => {
  const { username } = route.params;

  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [success, setSuccess] = useState(false);
  const [blogText, setBlogText] = useState("");

  const handleAddBlog = async () => {
    try {
      const token = await AsyncStorage.getItem("token"); // protected user to make sure no other users can create if they are not authenticated
      if (token !== null) {
        const response = await fetch("http://192.168.10.123:5000/api/blogs", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({ title, content }),
        });
        const data = await response.json();

        if (title !== "" && content !== "") {
          setSuccess(true);
          setBlogText("BLOG ADDED");
        } else {
          setBlogText("PLEASE ADD ALL FIELDS");
        }

        // Timner for delete the success message after 3 seconds
        setTimeout(() => {
          setSuccess(false);
          setBlogText("");
        }, 3000);
      }
    } catch (error) {
      console.error(error);
    }

    // Clear the input fields after submitting
    setTitle("");
    setContent("");
  };

  console.log(blogText);

  return (
    <View style={styles.container}>
      <View>
        <Header username={username} />
      </View>
      <View style={styles.formSize}>
        <Text style={styles.titleText}>Create Blog:</Text>
        <TextInput
          style={styles.input}
          placeholder="Title"
          value={title}
          onChangeText={setTitle}
        />
        <TextInput
          placeholder="Content"
          value={content}
          onChangeText={setContent}
          multiline
          numberOfLines={4}
          style={styles.textArea}
        />
        <Button title="Add Blog" onPress={handleAddBlog} />
        {success ? (
          <Text style={styles.hiddenText}>{blogText}</Text>
        ) : (
          <Text style={styles.hiddenTextRed}>{blogText}</Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: "#ccc",
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 25,
    marginBottom: 15,
  },
  formSize: {
    marginRight: 20,
    marginLeft: 20,
    marginTop: 170,
  },
  textArea: {
    height: 200,
    textAlignVertical: "top",
    borderWidth: 1,
    borderColor: "#ccc",
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  hiddenText: {
    marginTop: 20,
    fontSize: 18,
    paddingLeft: 10,
    borderLeftWidth: 5,
    borderLeftColor: "green",
  },
  hiddenTextRed: {
    marginTop: 20,
    fontSize: 18,
    paddingLeft: 10,
    color: "red",
  },
});

export default CreateBlogPage;
