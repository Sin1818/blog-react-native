import { useNavigation } from "@react-navigation/native";
import React from "react";
import { View, StyleSheet, Button, Dimensions, Text } from "react-native";

const StartPage = () => {
  const nav = useNavigation();

  const handleRegister = () => {
    nav.navigate("Register");
  };

  const handleLogin = () => {
    nav.navigate("Log In");
  };

  return (
    <View style={styles.container}>
      <View style={styles.titleView}>
        <Text style={styles.title}>Blog App</Text>
      </View>
      <Button
        title="Log In"
        style={styles.buttonBorders}
        onPress={() => handleLogin()}
      />
      <View style={styles.hrStyle} />
      <Button title="Sign Up" onPress={() => handleRegister()} />
      <Text style={styles.Sentence}>Sign Up/Log in to create a blog</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    width: "60%",
    marginLeft: "auto",
    marginRight: "auto",
  },
  hrStyle: {
    marginVertical: 20,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "auto",
    marginRight: "auto",
  },
  buttonBorders: {
    marginVertical: 8,
  },
  titleView: {
    width: "100%",
    marginBottom: 80,
  },
  title: {
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold",
  },
  Sentence: {
    marginLeft: "auto",
    marginRight: "auto",
    fontSize: 15,
    fontWeight: "bold",
    marginTop: 25,
  },
});

export default StartPage;
