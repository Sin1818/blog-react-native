import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Dimensions,
  Text,
} from "react-native";

const RegisterForm = ({ navigation }) => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleRegister = async () => {
    try {
      // connecting to backend to send the user info
      const response = await fetch("http://192.168.10.123:5000/api/users", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ name, password, email }),
      });
      const data = await response.json();

      AsyncStorage.setItem("token", data.token);
      console.log("Token stored successfully");

      if (data.token) {
        navigation.navigate({
          name: "Blog",
          params: { username: data.name, token: data.token },
          merge: false, // Here you can choose whether to merge or overwrite parmas
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  const linkLogin = () => {
    navigation.navigate("Log In");
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Sign Up</Text>
      <TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={setEmail}
        value={email}
      />
      <TextInput
        style={styles.input}
        placeholder="User name"
        onChangeText={setName}
        value={name}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        secureTextEntry
        onChangeText={setPassword}
        value={password}
      />
      <Button title="Sign Up" onPress={handleRegister} />
      <Text style={styles.loginText}>
        Already a user?{" "}
        <Text onPress={() => linkLogin()} style={styles.blueText}>
          Log in
        </Text>
      </Text>
    </View>
  );
};

const { width, height } = Dimensions.get("window");
const containerTop = height * 0.2; // Calculate 20% of the screen height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: containerTop,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 16,
    paddingHorizontal: 10,
    width: width - 32, // Subtracting the horizontal padding to fit the screen width
  },
  titleText: {
    justifyContent: "center",
    fontSize: 26,
    fontWeight: "bold",
    marginBottom: 16,
  },
  loginText: {
    justifyContent: "center",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 25,
  },
  blueText: {
    color: "blue",
  },
});

export default RegisterForm;
