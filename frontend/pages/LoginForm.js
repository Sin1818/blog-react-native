import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Dimensions,
  Text,
} from "react-native";

const LoginForm = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async () => {
    try {
      // connecting to backend to get the user
      const response = await fetch(
        "http://192.168.10.123:5000/api/users/login",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ email, password }),
        }
      );
      const data = await response.json();

      AsyncStorage.setItem("token", data.token); // Storing the token
      console.log("Token stored successfully");

      // Navigate to users blog page after login
      if (data.token) {
        navigation.navigate({
          name: "Blog",
          params: { username: data.name, token: data.token }, //passing username and token to blog page (Token have been stored so no need for this but use incase you just want pass the token)
          merge: false,
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  const nav = useNavigation();

  const linkRegister = () => {
    nav.navigate("Register");
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Log in</Text>
      <TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={setEmail}
        value={email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        secureTextEntry
        onChangeText={setPassword}
        value={password}
      />
      <Button title="Log In" onPress={handleLogin} />
      <Text style={styles.loginText}>
        Not a user?{" "}
        <Text onPress={() => linkRegister()} style={styles.blueText}>
          Sign Up
        </Text>{" "}
      </Text>
    </View>
  );
};

const { width, height } = Dimensions.get("window");
const containerTop = height * 0.2; // Calculate 20% of the screen height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: containerTop,
  },
  input: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 16,
    paddingHorizontal: 10,
    width: width - 32, // Subtracting the horizontal padding to fit the screen width
  },
  titleText: {
    justifyContent: "center",
    fontSize: 26,
    fontWeight: "bold",
    marginBottom: 16,
  },
  loginText: {
    justifyContent: "center",
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 25,
  },
  blueText: {
    color: "blue",
  },
});

export default LoginForm;
