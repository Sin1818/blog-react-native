import StartPage from "./pages/StartPage";
import RegisterFrom from "./pages/RegisterForm";
import LoginForm from "./pages/LoginForm";
import BlogPage from "./pages/BlogPage";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CreateBlogPage from "./pages/CreateBlogPage";
import AllUsersBlogs from "./pages/AllUsersBlogs";

export default function App() {
  const StackedNavigation = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <StackedNavigation.Navigator>
        <StackedNavigation.Screen
          name="Home"
          component={StartPage}
          options={{ headerShown: false }}
        />
        <StackedNavigation.Screen name="Register" component={RegisterFrom} />
        <StackedNavigation.Screen name="Log In" component={LoginForm} />
        <StackedNavigation.Screen
          name="CreateBlog"
          component={CreateBlogPage}
          options={{ headerShown: false }}
        />
        <StackedNavigation.Screen
          name="Blog"
          component={BlogPage}
          options={{ headerShown: false }}
        />
        <StackedNavigation.Screen
          name="AllBlogs"
          component={AllUsersBlogs}
          options={{ headerShown: false }}
        />
      </StackedNavigation.Navigator>
    </NavigationContainer>
  );
}
